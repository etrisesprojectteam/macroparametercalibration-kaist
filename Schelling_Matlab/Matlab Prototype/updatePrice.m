function price = updatePrice(inWorld, inPrice, size, priceWeight)
    price = inPrice;

    for i = 1 : size
        for j = 1: size
            price = updatePriceXY(i,j, inWorld(i,j), price, size, priceWeight);
        end
    end
end