function [distance idx] = distanceToNearestDowntown(downtown, x,y)
    distanceList = zeros(1,length(downtown));
    [length1 temp] = size(downtown);
    for i = 1 : length1
        distanceList(i) = abs(downtown(i,1) - x) + abs(downtown(i,2) - y);
    end
    idx = find(distanceList == min(distanceList),1);
    distance = distanceList(idx);
end