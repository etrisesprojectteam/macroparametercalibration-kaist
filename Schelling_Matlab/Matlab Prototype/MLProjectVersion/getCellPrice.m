function cellPrice = getCellPrice(x, y , size, price)
    currentPos = (y-1) * size + x;
    cellPrice =  price(currentPos);
end