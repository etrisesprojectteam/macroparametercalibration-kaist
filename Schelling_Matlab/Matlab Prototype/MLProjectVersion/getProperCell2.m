function [candX, candY, vac, isCandExist] = getProperCell2(x,y,world,price,vacantSet,numVacantCell, similarRatio, neighborType, worldSize, happiness, downtown)
    % VACANT Set 전체에 대해 score를 산출하고, 해당 score를 기준으로
    % 각 agent type별 utility를 계산하여 가장 적합한 위치 결정
    
    candScoreRich = zeros(1,length(vacantSet));
    candScorePoor = zeros(1, length(vacantSet));
    
    for i = 1:length(vacantSet)
        [happiness unhappyRatio] = isHappy(vacantSet(i,1), vacantSet(i,2), world, world(x,y), similarRatio, neighborType, worldSize);
        candPrice = price(vacantSet(i,1), vacantSet(i,2));
        distanceDown = distanceToNearestDowntown(downtown, vacantSet(i,1), vacantSet(i,2));
        
        candScoreRich(i) = ((1 / distanceDown) + 0.1)^(candPrice) + (unhappyRatio)^(candPrice);
        % candScorePoor(i) = ((1 / distanceDown) + 0.1)^(1/candPrice) + (unhappyRatio)^(1/candPrice);
    end
    
    if(world(x,y) == 1) % Rich
        cand = find(candScoreRich == max(candScoreRich),1);
        candX = vacantSet(cand,1); candY = vacantSet(cand,2);
        vac = cand;
        isCandExist = 1;
    elseif(world(x,y) == 2) % Poor
        cand = find(candScoreRich == min(candScoreRich),1);
        candX = vacantSet(cand,1); candY = vacantSet(cand,2);
        vac = cand;
        isCandExist = 1;
    end
end