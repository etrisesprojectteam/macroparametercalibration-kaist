function [output] = SABM_UrbanSuite()
    %% Input Parameters
    worldSize = 50;
    unitTime = 5;
    populationRatio = 100;
    richPoorRatio = 0.5;
    richQualityPriority = -1;
    poorPricePriority = -1;
    
    currentCellWeight = 0.005 ;
    weightDiff = 0.002 ;
    
    startYear = 1998;
    endYear = 2015;
    
    serviceLevelRange = 5;
    
    %% Population Data Load
    [initPopulation, totalPopulation, populationChange] = populationDataLoad(worldSize, populationRatio, startYear, endYear); % Output: (1) initPopulation (2) totalPopulation
    maxPopulation = max(max(initPopulation));

    %% Agent/Environment Initialization
    world = cell(worldSize, worldSize);
    agentList = cell(1, totalPopulation);
    checkIdx = 1;
    
    % Population Data에 근거하여, Rich/Poor Agent들을 World에 배치하는 단계
    for i = 1:worldSize
        for j = 1:worldSize
            [tempGrid, tempAgentList] = agentGridInitialize(initPopulation(i,j), richPoorRatio, i, j);
            
            world{i,j} = tempGrid;
            [a, length] = size(tempAgentList);
            agentList(checkIdx:checkIdx+length-1) = tempAgentList;
            checkIdx = checkIdx+length;
        end
    end
    
    % (1) 분포된 Rich/Poor에 따라, 초기 100으로 설정된 LandPrice/LandQuality를 재조정 (Von
    % Neuman Neighborhood, (+), Adjacency:1
    
    % (2) Population 유무에 따라, service 존재 유무 설정
    serviceMarking = zeros(worldSize, worldSize);
    for i = 1:worldSize
        for j = 1:worldSize
            % 현재 Cell
            world{i,j}.landPrice = world{i,j}.landPrice * (1 + currentCellWeight)^(world{i,j}.numRich) * (1 - currentCellWeight)^(world{i,j}.numPoor);
            world{i,j}.landQuality = world{i,j}.landQuality * (1 + currentCellWeight)^(world{i,j}.numRich) * (1 - currentCellWeight)^(world{i,j}.numPoor);
            % Adjacency:1
            [numRichAdj, numPoorAdj] = getAdjacencyInfo(world,worldSize,i,j);
            world{i,j}.landPrice = world{i,j}.landPrice * (1 + currentCellWeight - (weightDiff * 1))^(numRichAdj) * (1 - currentCellWeight + (weightDiff * 1))^(numPoorAdj);
            world{i,j}.landQuality = world{i,j}.landQuality * (1 + currentCellWeight - (weightDiff * 1))^(numRichAdj) * (1 - currentCellWeight + (weightDiff * 1))^(numPoorAdj);
            
            if(world{i,j}.getNumRich()+world{i,j}.getNumPoor() > 0)
                world{i, j}.setExistService(1);
                world{i, j}.setSdDist(0);
                
                tempLevel = serviceLevelCal(maxPopulation, serviceLevelRange, (world{i, j}.getNumRich() + world{i,j}.getNumPoor()));
                world{i,j}.setServiceLevel(tempLevel);
                
                serviceMarking(i,j) = 1;
            end
        end
    end
    
    % TODO: 현재 배치된 Agent 수/특성에 비례하여 ServiceLocate를 결정
    for i = 1:worldSize
        for j = 1:worldSize
            if(world{i, j}.getExistService() == 0)
                [dist sLevel] = findMinimumServiceGrid(world, worldSize, i, j, serviceMarking);
                world{i, j}.setSdDist(dist);
                world{i, j}.setServiceLevel(sLevel);
            end
        end
    end
  
    % Plot Initial World
    figurePlot(world, worldSize, startYear, maxPopulation);
    figurePlot2(world, worldSize, startYear, maxPopulation);
    %% Simulation Run
    for simTime = 1:( endYear - startYear )
    %for simTime = 1:3
        if(simTime ~= 1)
            % TODO: Adopt population changing variables
            % (1) Adopt serviceExist and serviceLevel as input parameter
            % (Daejeon population data)
            paramMaxPopulation = max(max(populationChange(:,:,simTime-1))); % simTime-1이 되어야함
            serviceMarking = zeros(worldSize, worldSize);
            for i = 1:worldSize
                for j = 1:worldSize
                    if(populationChange(i,j,simTime) > 0)
                        world{i, j}.setExistService(1);
                        world{i, j}.setSdDist(0);

                        tempLevel = serviceLevelCal(paramMaxPopulation, serviceLevelRange, populationChange(i,j,simTime-1));
                        world{i,j}.setServiceLevel(tempLevel);

                        serviceMarking(i,j) = 1;
                    end
                end
            end

            % (2) Re-setting serviceLocate
            for i = 1:worldSize
                for j = 1:worldSize
                    if(world{i, j}.getExistService() == 0)
                        [dist sLevel] = findMinimumServiceGrid(world, worldSize, i, j, serviceMarking);
                        world{i, j}.setSdDist(dist);
                        world{i, j}.setServiceLevel(sLevel);
                    end
                end
            end
        end
        
        % Calculate current grid score for rich / poor agents
        scoreForRich = zeros(1,worldSize^2);
        scoreForPoor = zeros(1,worldSize^2);
        
        for i = 1:worldSize
            for j = 1:worldSize
                % scoreForRich(50*(j-1)+i) = (1+world{i,j}.landQuality);
                scoreForRich(50*(i-1)+j) = ( (1/(world{i,j}.getSdDist()/10 +0.1)) * (0.1 * world{i, j}.getServiceLevel()) )^(1 - richQualityPriority) * (1+world{i,j}.landQuality)^(1+richQualityPriority);
                scoreForPoor(50*(i-1)+j) = ( (1/(world{i, j}.getSdDist()/10 + 0.1)) * (0.1 * world{i, j}.getServiceLevel()) )^(1-poorPricePriority) * (1/world{i,j}.landPrice)^(1+poorPricePriority);
            end
        end
        totalSumRich = sum(scoreForRich);           totalSumPoor = sum(scoreForPoor);
        scoreForRich = scoreForRich/totalSumRich;   scoreForPoor = scoreForPoor/totalSumPoor;

        for agt = 1:totalPopulation
            if(agentList{agt}.getIsRich()) % If the Chosen Agent Rich
                [candX, candY] = findBestPlaceRich(world, worldSize, richQualityPriority, scoreForRich);
                
                tempX = agentList{agt}.getCurrentPosX();
                tempY = agentList{agt}.getCurrentPosY();
                
%                if(scoreForRich(50*(tempX-1)+tempY) > scoreForRich(50*(candX-1)+candY))
%                    continue;
%                else

                    world{tempX, tempY}.setNumRich(world{tempX, tempY}.getNumRich - 1);
                    world{candX, candY}.setNumRich(world{candX, candY}.getNumRich + 1);

                    agentList{agt}.setCurrentPosX(candX);
                    agentList{agt}.setCurrentPosY(candY);

                    world = updateEnvironment(world, worldSize, candX, candY, agentList{agt}.getIsRich(), currentCellWeight, weightDiff);
%                end
            else % If the Chosen Agent Poor
                [candX, candY] = findBestPlacePoor(world, worldSize, poorPricePriority, scoreForPoor);
                
                tempX = agentList{agt}.getCurrentPosX();
                tempY = agentList{agt}.getCurrentPosY();
                
%                if(scoreForPoor(50*(tempX-1)+tempY) > scoreForPoor(50*(candX-1)+candY))
%                    continue;
%                else
                
                    world{tempX, tempY}.setNumPoor(world{tempX, tempY}.getNumPoor - 1);
                    world{candX, candY}.setNumPoor(world{candX, candY}.getNumPoor + 1);

                    agentList{agt}.setCurrentPosX(candX);
                    agentList{agt}.setCurrentPosY(candY);

                    world = updateEnvironment(world, worldSize, candX, candY, agentList{agt}.getIsRich(), currentCellWeight, weightDiff);
 %               end
            end
            sprintf('current itr: %d, %d/%d', simTime, agt, totalPopulation)
        end
        tempPopulation = zeros(1,worldSize^2);
        for i = 1:worldSize
            for j = 1:worldSize
                tempPopulation((j-1)*worldSize+i) = world{i,j}.getNumRich() + world{i,j}.getNumPoor();
            end
        end
        tempMaxPopulation = max(max(tempPopulation));
        figurePlot(world, worldSize, startYear + simTime, tempMaxPopulation);
        figurePlot2(world, worldSize, startYear + simTime, tempMaxPopulation);
        
        %fileName = strcat(num2str(startYear+simTime-1),'_',datestr(clock, 'MM_SS'),'.mat');
        %save strcat(num2str(s),'_',datestr(clock, 'MM_SS'),'.txt')

    end
end