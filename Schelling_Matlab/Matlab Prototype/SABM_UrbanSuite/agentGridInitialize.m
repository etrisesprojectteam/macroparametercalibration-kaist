function [tempGrid, tempAgentList] = agentGridInitialize(gridPopulation, richPoorRatio, x, y)
    tempGrid = Grid();
    tempAgentList = cell(1,gridPopulation);
    
    for i = 1:gridPopulation
        tempAgent = Agent();
        
        coinToss = rand();
        if(coinToss > richPoorRatio) % richPoorRatio: Rich와 Poor의 비율 조정. 높을 수록 Rich가 적어짐. 즉 낮을 수록 Poor 비율 증가
            tempAgent.setIsRich(1);
            tempGrid.setNumRich(tempGrid.getNumRich()+1);
            tempAgent.setCurrentPosX(x);
            tempAgent.setCurrentPosY(y);
        else
            tempAgent.isRich = 0;
            tempGrid.setNumPoor(tempGrid.getNumPoor()+1);
            tempAgent.setCurrentPosX(x);
            tempAgent.setCurrentPosY(y);
        end
        tempAgentList{1, i} = tempAgent;
    end
end