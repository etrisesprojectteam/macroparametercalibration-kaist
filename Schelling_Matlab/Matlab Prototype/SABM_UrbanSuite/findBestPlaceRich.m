function [candX, candY] = findBestPlaceRich(world, worldSize, richQualityPriority, scoreForRich)
%    scoreForRich = zeros(1,worldSize^2);
    candX = 0; candY = 0;

%    for i = 1:worldSize
%        for j = 1:worldSize
%            % scoreForRich(50*(j-1)+i) = (1+world{i,j}.landQuality);
%            scoreForRich(50*(j-1)+i) = ( (1/(world{i,j}.getSdDist()+0.1)) * (0.1 * world{i, j}.getServiceLevel()) )^(1 - richQualityPriority) * (1+world{i,j}.landQuality)^(1+richQualityPriority);
%        end
%    end
%    totalSum = sum(scoreForRich);
%    scoreForRich = scoreForRich/totalSum;
    
    coinToss = rand();
    if(coinToss == 0)
        candX = 1;
        candY = 1;
    elseif(coinToss == 1)
        candX = worldSize;
        candY = worldSize;
    else
        tempSum = 0;
%        for i = 1:worldSize
%            for j = 1:worldSize
%                tempSum = tempSum + scoreForRich(50*(j-1)+i);
%                if(tempSum > coinToss)
%                    candX = i;
%                    candY = j;
%                    break;
%                end
%            end
%            if((candX ~= 0) && (candY ~=0))
%                break;
%            end
%        end
        for i = 1:worldSize
            tempSum = tempSum+sum(scoreForRich(1+50*(i-1):50*i));
            if(tempSum>coinToss)
                candX = i;
                tempSum = tempSum-sum(scoreForRich(1+50*(i-1):50*i));
                for j = 1:worldSize
                    tempSum = tempSum + scoreForRich(50*(i-1)+j);
                    if(tempSum > coinToss)
                        candY = j;
                        break;
                    end
                end
                if((candX ~= 0) && (candY ~=0))
                    break;
                end
            end
        end
    end
end