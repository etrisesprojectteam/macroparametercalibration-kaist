classdef Agent < handle
    properties
        isRich
        currentPositionX = 0;
        currentPositionY = 0;
    end
    methods
        %constructor
        function setIsRich(this,isRich)
            this.isRich = isRich;
        end
        
        function setCurrentPosX(this,currentPosX)
            this.currentPositionX = currentPosX;
        end
        
        function setCurrentPosY(this,currentPosY)
            this.currentPositionY = currentPosY;
        end
        
        function output = getIsRich(this)
            output = this.isRich;
        end
        
        function output = getCurrentPosX(this)
            output = this.currentPositionX;
        end
        
        function output = getCurrentPosY(this)
            output = this.currentPositionY;
        end
        
    end
end
