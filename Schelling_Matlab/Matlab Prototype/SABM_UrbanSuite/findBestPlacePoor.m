function [candX, candY] = findBestPlacePoor(world, worldSize, poorPricePriority, scoreForPoor)
%    scoreForPoor = zeros(1,worldSize^2);
    candX = 0; candY = 0;
    
%    for i = 1:worldSize
%        for j = 1:worldSize
            % scoreForPoor(50*(j-1)+i) = (1/world{i,j}.landPrice);
%            scoreForPoor(50*(j-1)+i) = ( (1/(world{i, j}.getSdDist()/100 + 0.1)) * (0.1 * world{i, j}.getServiceLevel()) )^(1-poorPricePriority) * (1/world{i,j}.landPrice)^(1+poorPricePriority);
%        end
%    end
%    totalSum = sum(scoreForPoor);
%    scoreForPoor = scoreForPoor/totalSum;
    
    coinToss = rand();
    if(coinToss == 0)
        candX = 1;
        candY = 1;
    elseif(coinToss == 1)
        candX = worldSize;
        candY = worldSize;
    else
        tempSum = 0;

%        for i = 1:worldSize
%            for j = 1:worldSize
%                tempSum = tempSum + scoreForPoor(50*(j-1)+i);
%                if(tempSum > coinToss)
%                    candX = i;
%                    candY = j;
%                    break;
%                end
%            end
%            if((candX ~= 0) && (candY ~=0))
%                break;
%            end
%        end
         for i = 1:worldSize
            tempSum = tempSum+sum(scoreForPoor(1+50*(i-1):50*i));
            if(tempSum>coinToss)
                candX = i;
                tempSum = tempSum-sum(scoreForPoor(1+50*(i-1):50*i));
                for j = 1:worldSize
                    tempSum = tempSum + scoreForPoor(50*(i-1)+j);
                    if(tempSum > coinToss)
                        candY = j;
                        break;
                    end
                end
                if((candX ~= 0) && (candY ~=0))
                    break;
                end
            end
         end
    end 
end