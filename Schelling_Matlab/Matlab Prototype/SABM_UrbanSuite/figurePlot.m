function figurePlot(world, worldSize, year, maxPopulation)
    figure;
    X = zeros(worldSize, worldSize);

    for i = 1:worldSize
        for j = 1:worldSize
            X(i,j) = world{i,j}.getNumRich() + world{i,j}.getNumPoor();
        end
    end


    colormap('jet')
    imagesc(X);

    h=colorbar;
    caxis([0 maxPopulation]) % total
    % caxis([0 5000]) % only positive change
    % caxis([5000 10000]) % only negative change

    title([ 'Year ' num2str(year) ]);
end

