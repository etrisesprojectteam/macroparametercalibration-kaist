function serviceLevel = serviceLevelCal(maxPopulation, serviceLevelRange, currentPopulation)
    boundary = ceil(maxPopulation/serviceLevelRange);
    % Max population을 가지는 grid의 service level을 max로 설정 (as
    % serviceLevelRange), 인구 0의 grid의 service level을 0으로 설정
    
    for i=1:serviceLevelRange
        if(currentPopulation < boundary * i)
            serviceLevel = i+1;
            break;
            % 현재 grid의 population의 serviceLevel을 return
        end
        serviceLevel = serviceLevelRange;
    end 
end