function [dist,sLevel] = findMinimumServiceGrid(world, worldSize, inputX, inputY, serviceMarking)
distanceRecorder = ones(1, worldSize^2) * 99999;
    for i = 1:worldSize
        for j = 1:worldSize
            if(serviceMarking(i,j) == 1)
                distanceRecorder(worldSize*(j-1) + i) = abs(inputX - i) + abs(inputY - j);
            end
        end
    end
    idx = find(distanceRecorder == min(distanceRecorder), 1);

    tempX = floor(idx/worldSize)+1;
    tempY = mod(idx, worldSize);
    if(tempY == 0)
        tempY = worldSize;
    end
    
    sLevel = world{tempX, tempY}.getServiceLevel();
    dist = distanceRecorder(idx);
end