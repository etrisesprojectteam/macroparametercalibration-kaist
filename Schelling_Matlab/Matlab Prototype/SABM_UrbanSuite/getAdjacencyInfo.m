function [numRichAdj, numPoorAdj] = getAdjacencyInfo(world,worldSize,i,j)
    tempNumRich = 0;
    tempNumPoor = 0;
    
    if( i-1 > 0 )
        tempNumRich = tempNumRich + world{i-1,j}.numRich;
        tempNumPoor = tempNumPoor + world{i-1,j}.numPoor;
    end
    
    if(j-1 > 0)
        tempNumRich = tempNumRich + world{i, j-1}.numRich;
        tempNumPoor = tempNumPoor + world{i, j-1}.numPoor;
    end
    
    if(j+1 < worldSize+1)
        tempNumRich = tempNumRich + world{i, j+1}.numRich;
        tempNumPoor = tempNumPoor + world{i, j+1}.numPoor;
    end
    
    if(i+1 < worldSize+1)
        tempNumRich = tempNumRich + world{i+1, j}.numRich;
        tempNumPoor = tempNumPoor + world{i+1, j}.numPoor;
    end

    numRichAdj = tempNumRich;
    numPoorAdj = tempNumPoor;
end