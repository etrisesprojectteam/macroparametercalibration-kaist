classdef Grid < handle
    properties
        numRich = 0;
        numPoor = 0;
        existService = 0;
        sdDist = 0;
        serviceLevel = 0;
        landPrice = 40;
        landQuality = 40;
    end
    
    methods
        % set functions
        function setNumRich(this, numRich)
            this.numRich = numRich;
        end
        
        function setNumPoor(this, numPoor)
            this.numPoor = numPoor;
        end
        
        function setExistService(this, boolSer)
            this.existService = boolSer;
        end
        
        function setSdDist(this, sdDist)
            this.sdDist = sdDist;
        end
        
        function setServiceLevel(this, serviceLevel)
            this.serviceLevel = serviceLevel;
        end
        
        function setLandPrice(this, landPrice)
            this.landPrice = landPrice;
        end
        
        function setLandQuality(this, landQuality)
            this.landQuality = landQuality;
        end
        
        % get functions
        function output = getNumRich(this)
            output = this.numRich;
        end
        
        function output = getNumPoor(this)
            output = this.numPoor;
        end
        
        function output = getExistService(this)
            output = this.existService;
        end
        
        function output = getSdDist(this)
            output = this.sdDist;
        end
        
        function output = getServiceLevel(this)
            output = this.serviceLevel;
        end
        
        function output = getLandPrice(this)
            output = this.landPrice;
        end
        
        function output = getLandQuality(this)
            output = this.landQuality;
        end
    end
end