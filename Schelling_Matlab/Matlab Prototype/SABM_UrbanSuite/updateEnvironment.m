function [ updatedWorld ] = updateEnvironment( world, worldSize, candX, candY, isRich, currentCellWeight, weightDiff )
    if(isRich)
        world{candX, candY}.landQuality = world{candX, candY}.landQuality * (1 + currentCellWeight);
        
        if(candX-1 > 0)
            world{candX-1, candY}.landQuality = world{candX-1, candY}.landQuality * (1 + currentCellWeight - weightDiff);
        end
        
        if(candY-1 > 0)
            world{candX, candY-1}.landQuality = world{candX, candY-1}.landQuality * (1 + currentCellWeight - weightDiff);
        end
        
        if(candY+1 < worldSize+1)
            world{candX, candY+1}.landQuality = world{candX, candY+1}.landQuality * (1 + currentCellWeight - weightDiff);
        end
        
        if(candX+1 < worldSize+1)
            world{candX+1, candY}.landQuality = world{candX+1, candY}.landQuality * (1 + currentCellWeight - weightDiff);
        end
        updatedWorld = world;
    else
        world{candX, candY}.landPrice = world{candX, candY}.landPrice * (1 - currentCellWeight);
        
        if(candX-1 > 0)
            world{candX-1, candY}.landPrice = world{candX-1, candY}.landPrice * (1 - currentCellWeight + weightDiff);
        end
        
        if(candY-1 > 0)
            world{candX, candY-1}.landPrice = world{candX, candY-1}.landPrice * (1 - currentCellWeight + weightDiff);
        end
        
        if(candY+1 < worldSize+1)
            world{candX, candY+1}.landPrice = world{candX, candY+1}.landPrice * (1 - currentCellWeight + weightDiff);
        end
        
        if(candX+1 < worldSize+1)
            world{candX+1, candY}.landPrice = world{candX+1, candY}.landPrice * (1 - currentCellWeight + weightDiff);
        end
        updatedWorld = world;
    end
end

