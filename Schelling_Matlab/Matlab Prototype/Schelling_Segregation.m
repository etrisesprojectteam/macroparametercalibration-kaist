classdef Schelling_Segregation < handle
    properties
        simTime % proceeding simulation time
        worldSize % environment size
        vacantRatio % rate of vacant space in the world
        simillarRatio % rate of similar wanted in a person's neighborhood
        populationRatio % proportion of the two populations in the world
        neighborType % 1: Neuman (8), 2: Moore (4)
    end
    
    methods
        %% Parameter Initialization
        
        function this = Schelling_Segregation()
            this.simTime = 100;
            this.worldSize = 45;
            this.vacantRatio = 0.10;
            this.simillarRatio = 0.6;
            this.populationRatio = 0.5;
            this.neighborType = 1;
        end
        
        %% Execute Real Segregation Simulation
        
        function run(this)
            size  = this.worldSize;
            [world,vacantSet,numVacantCell] = this.getInitialRandomWorld();
            moved = zeros(size);
            this.plotWorld(world); % Show Randomly initialized world of Schelling's Model
            
            for t = 1:this.simTime % Running for designated simulation time (TODO, d(totalSatisfactory) < epsilon form)
                disp(t);
                
                % RandomItr Case
                idx1 = randperm(size);
                idx2 = randperm(size);
                
                for i = 1 : size
                    for j = 1 : size
                        x = idx1(mod(i+j,size)+1); y = idx2(j);
                        if(~moved(x,y) && world(x,y)) % 해당 cell이 T=t_current에 변경된 적이 없고, 어떤 agent가 존재한다면
                            if(~this.isHappy(x,y,world, world(x,y))) % 해당 Agent가 현재 상황에 만족하지 않는다면
                                [candX,candY,vac] = this.getProferCell(x,y,world,vacantSet,numVacantCell);
                                
                                moved(x,y) = 1;
                                moved(candX,candY) = 1;
                                temp = world(x,y);
                                world(x,y) = world(candX, candY);
                                world(candX, candY) = temp;
                                
                                vacantSet(vac, 1) = x;
                                vacantSet(vac, 2) = y;
                                
                                this.plotWorld(world);
                            end
                        end
                    end
                end
                moved = zeros(size);
            end
        end
        
        %% Further functions for implementation
        
        function [initworld,vacantSet, numVacantCell] = getInitialRandomWorld(this)
            numVacantCell = 0;
            numPop1 = 0;
            
            size = this.worldSize;
            initworld = zeros(size);
            
            idx1 = randperm(size);
            idx2 = randperm(size);
            
            for i = 1:size
                for j = 1:size
                    if(rand(1) > this.vacantRatio)
                        if(rand(1) > this.populationRatio)
                            initworld(idx1(i),idx2(j)) = 1;
                            numPop1 = numPop1 + 1;
                        else
                            initworld(idx1(i),idx2(j)) = 2;
                        end
                    else
                        numVacantCell = numVacantCell +1;
                        vacantSet(numVacantCell,:) = [idx1(i),idx2(j)];
                    end
                end
            end
            realVacantRatio = numVacantCell / (size*size)
            realPopulationRatio = numPop1 / (size*size)
        end % Initial Random World Loader
        
        function plotWorld(this,world)
            imagesc(world, [0,2]);
            colormap([1 1 1; 0,0,1;1,0,0]);
            pause(0.01);
        end % 현재까지 Update된 World를 plot
        
        function happiness = isHappy(this,x,y,world, agentType)
            [numNeigh, numRed] = this.getNeighborInfo(x,y,world);
            if(agentType ==1)
                numBlue = numNeigh - numRed;
                if(numBlue/numNeigh > this.simillarRatio)
                    happiness = 0;
                else
                    happiness = 1;
                end
            else
                if(numRed/numNeigh > this.simillarRatio)
                    happiness = 0;
                else
                    happiness = 1;
                end
            end
        end % world(x,y)에 agentType의 Agent가 위치한다면, 이 Agent는 만족할것인가?
        
        function [numOfNeighbor, numOfRedAgent] = getNeighborInfo(this,x,y,world)
            if(this.neighborType==1) % 1: Neuman (8), Only consider this case for this version.
                neighbor = [x-1 y+1;x y+1;x+1 y+1;x-1 y;x+1 y;x-1 y-1;x y-1;x+1 y-1];
                valid = [1;1;1;1;1;1;1;1];
                numOfRedAgent = 0;
                
                for i = 1:8 % Only take in-world exist cell
                    if((sum(neighbor(i,:) < 1)>0) || (sum(neighbor(i,:) > this.worldSize)>0))
                        valid(i) = 0;
                    else
                        if(world(neighbor(i,1), neighbor(i,2))==1)
                            numOfRedAgent = numOfRedAgent + 1;
                        end
                    end
                end
                numOfNeighbor = sum(valid);
            else % 2: Moore (4)
            end
        end % (x,y) 위치의 Agent에 neighbor 정보를 return
        
        function [candX, candY, vac] = getProferCell(this, x,y, world, vacantSet,numVacantCell) % (x,y) Agent가 이동할 수 있는 최적의 위치 확인
            len = zeros(numVacantCell);     len = len(1,:);
            
            for i = 1:length(vacantSet)
                if(this.isHappy(vacantSet(i,1),vacantSet(i,2),world,world(x,y))) % 빈 칸 (world 전체 중 VacantSet)으로 이동했을 때, 행복한지 아닌지에 대한 boolean List
                    len(i) = (abs(x-vacantSet(i,1)) + abs(y-vacantSet(i,2))); % 모든 빈 칸 Set에 대해 현재 이동하는 Agent와의 거리
                else
                    len(i) = this.worldSize*2;
                end
            end % 결과로 나오는 len Vector는 이동했을때 행복한 vacant cell에 대해서만! 거리 표시하는 vector (나머지는 최대~값)
            
            candX = vacantSet(find(len==min(len), 1),1);
            candY = vacantSet(find(len==min(len), 1),2);
            
            vac = find(len==min(len), 1);
        end
    end
end
