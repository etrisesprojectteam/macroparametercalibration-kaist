function happinessNearness = isHappyJob(x,y,downtown, nearness)
    [distance idx] = distanceToNearestDowntown(downtown, x, y);
    
    if(nearness < distance)
        happinessNearness = 0;
    else
        happinessNearness = 1;
    end
end