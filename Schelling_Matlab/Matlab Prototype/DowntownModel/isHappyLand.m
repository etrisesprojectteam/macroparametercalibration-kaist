function [happinessLand] = isHappyLand(x,y,world,price,size)
% Whether higher/lower than mean price of neighbor
    currentPos = (y-1)*size+x;
    distOneNeigh = [currentPos-1, currentPos+1, currentPos-size, currentPos+size];
    distTwoNeigh = [currentPos-size-1, currentPos-size+1, currentPos+size-1, currentPos+size+1];
    
    if(x == 1)
        distOneNeigh(1) = -size-1; distTwoNeigh([1,3]) = -size-1;
    end
    
    if(y == 1)
        distOneNeigh(3) = -size-1; distTwoNeigh([1,2]) = -size-1;
    end
    
    if(x == size)
        distOneNeigh(2) = -size-1; distTwoNeigh([2,4]) = -size-1;
    end
    
    if(y == size)
        distOneNeigh(4) = -size-1; distTwoNeigh([3,4]) = -size-1;
    end
    
    distOneNeigh(find(distOneNeigh == -size-1)) = [];
    distTwoNeigh(find(distTwoNeigh == -size-1)) = [];
    
    neighbor = [currentPos, distOneNeigh, distTwoNeigh];
    
    meanPrice = mean(mean(price(neighbor)));
    
    if(price(currentPos) < meanPrice)
        if(world(x,y)==1)
            happinessLand = 0;
        else
            happinessLand = 1;
        end
    else
        if(world(x,y)==2)
            happinessLand = 1;
        else
            happinessLand = 0;
        end
    end     
end
