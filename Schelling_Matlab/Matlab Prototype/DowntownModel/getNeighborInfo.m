function [numOfNeighbor, numOfRedAgent] = getNeighborInfo(x,y,world,neighborType, worldSize)
	if(neighborType==1) % 1: Neuman (8), Only consider this case for this version.
        neighbor = [x-1 y+1;x y+1;x+1 y+1;x-1 y;x+1 y;x-1 y-1;x y-1;x+1 y-1];
        valid = [1;1;1;1;1;1;1;1];
        numOfRedAgent = 0;
                
        for i = 1:8 % Only take in-world exist cell
            if((sum(neighbor(i,:) < 1)>0) || (sum(neighbor(i,:) > worldSize)>0))
                valid(i) = 0;
            else
                if(world(neighbor(i,1), neighbor(i,2))==1)
                    numOfRedAgent = numOfRedAgent + 1;
                end
            end
        end
        numOfNeighbor = sum(valid);
	else % 2: Moore (4)
	end
end % (x,y) 위치의 Agent에 neighbor 정보를 return