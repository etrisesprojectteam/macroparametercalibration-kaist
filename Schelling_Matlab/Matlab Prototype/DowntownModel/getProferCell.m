% (x,y) Agent가 이동할 수 있는 최적의 위치 확인
function [candX, candY, vac, isCandExist] = getProferCell( x,y, world, price, vacantSet,numVacantCell,simillarRatio,neighborType, worldSize, happiness, downtown) 
	% 1) Race / Price 동시 고려
    % candPrice: 이동 후보가 될 수 있는 (지가/주변 인간 분류의 두 조건을 모두 만족시키는 위치에 대한 지가)
    % candCoord: 위치
    % vacCand: 위 위치를 저장하고 있는 vacantSet의 idx (본 function의 vac return value,
    %          이후 vacantSet Update를 위함
    % candLength: 동적 할당을 위한 변수
    
    % 2) Race만 고려
    % candDist
    % candCoord_race
    % vacCand_race
    % candLength_race
    
    candLength = 0; candLength_race = 0;
    
            
	for i = 1:length(vacantSet)
        if(isHappy(vacantSet(i,1),vacantSet(i,2),world,world(x,y),simillarRatio,neighborType,worldSize)) 
            if(isHappyLand(vacantSet(i,1),vacantSet(i,2),world,price,worldSize))
                % 빈 칸 (world 전체 중 VacantSet)으로 이동했을 때, 행복한지 아닌지에 대한 boolean List
                %len(i) = (abs(x-vacantSet(i,1)) + abs(y-vacantSet(i,2))); % 모든 빈 칸 Set에 대해 현재 이동하는 Agent와의 거리
                candPrice(candLength+1) = price(vacantSet(i,1),vacantSet(i,2));
                candCoord(candLength+1, 1) = vacantSet(i,1); candCoord(candLength+1, 2) = vacantSet(i,2);
                vacCand(candLength+1) = i;
                candLength = candLength+1;
            else
                candDist(candLength_race+1) = distanceToNearestDowntown(downtown, vacantSet(i,1),vacantSet(i,2));
                candCoord_race(candLength_race+1,1) = vacantSet(i,1); candCoord_race(candLength_race+1,2) = vacantSet(i,2);
                vacCand_race(candLength_race+1) = i;
                candLength_race = candLength_race+1;
            end
        end
	end % 결과로 나오는 vector들은 price에 의해서만 이동 후보를 뽑은 것들.

    if(candLength == 0) % 만약, price에 의해 이동할 수 있는 후보지가 존재하지 않는다면, 그냥 주변 Rich/Poor에 대해서만 후보지 정해서 할당하자. 후보 Cell에 대해 mindist만 고려.
        if(happiness) % price에 의해 더 나아질 수 있는 조건이 아니므로, 주변 race만 확인. 조건 만족한다면 continue
            candX = 0; candY = 0; vac = 0;
            isCandExist = 0;
        elseif(candLength_race == 0) % 주변 race만 고려하여 이동 후보지 뽑을 것, 근데 그럼에도 후보지가 없어?
            candX = 0; candY = 0; vac = 0;
            isCandExist = 0;
        else % 후보지가 있으면
            cand = find(candDist == min(candDist),1); % Downtown으로부터 거리가 가장 가까운 vacantCell을 선정하여 이동
            candX = candCoord_race(cand,1); candY = candCoord_race(cand,2);
            vac = vacCand_race(cand);
            isCandExist = 1;
            %fprintf('Rich/Poor 고려 위치 이동 (%d, %d) -> (%d, %d)\n', x,y, candX, candY);
        end
    else
        utilityForRich = zeros(1, candLength); utilityForPoor = zeros(1, candLength);
        for i = 1:candLength
            tempCandX = candCoord(i,1); tempCandY = candCoord(i,2);
            [dist idx] = distanceToNearestDowntown(downtown, tempCandX, tempCandY);
            utilityForRich(i) = ((1 / dist) + 0.1)^(candPrice(i));
            utilityForPoor(i) = ((1 / dist) + 0.1)^(1/candPrice(i));
        end
        if(world(x,y) == 1) % if rich
            %cand = find(candPrice==max(candPrice),1); % 조건을 만족하는 후보지 가운데, price가 가장 높은 곳의 idx
            cand = find(utilityForRich == max(utilityForRich),1); 
            candX = candCoord(cand, 1); candY = candCoord(cand, 2);
            vac = vacCand(cand);
            isCandExist = 1;
            %fprintf('Price 고려 위치 이동 (%d, %d) -> (%d, %d)\n', x,y, candX, candY);
        else % if poor
            %cand = find(candPrice==min(candPrice),1); % 조건을 만족하는 후보지 가운데, price가 가장 낮은 곳의 idx
            cand = find(utilityForPoor == min(utilityForPoor),1); 
            candX = candCoord(cand, 1); candY = candCoord(cand, 2);
            vac = vacCand(cand);
            isCandExist = 1;
            %fprintf('Price 고려 위치 이동 (%d, %d) -> (%d, %d)\n', x,y, candX, candY);
        end
    end
end