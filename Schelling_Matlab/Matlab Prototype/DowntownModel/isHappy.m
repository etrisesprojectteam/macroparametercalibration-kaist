function [happiness, unhappyRatio] = isHappy(x,y,world, agentType, simillarRatio,neighborType,worldSize)
	[numNeigh, numRed] = getNeighborInfo(x,y,world,neighborType,worldSize);
	
    if(agentType ==1)
        numBlue = numNeigh - numRed;
            if(numBlue/numNeigh > simillarRatio)
                happiness = 0;
                unhappyRatio = numBlue/numNeigh;
            else
                happiness = 1;
                unhappyRatio = numBlue/numNeigh;
            end
    else
        if(numRed/numNeigh > simillarRatio)
           happiness = 0;
           unhappyRatio = numRed/numNeigh;
	    else
            happiness = 1;
            unhappyRatio = numRed/numNeigh;
        end
	end
end % world(x,y)에 agentType의 Agent가 위치한다면, 이 Agent는 만족할것인가?