function HMM(x,EMitr,params)

    %% Variables
    p0=0.5; p1=1-p0; % Isp=[p0, p1]; % Initial State Probability
    pt00=0.5; pt01=1-pt00; pt10=0.5; pt11=1-pt10; Pt=[pt00 pt01;pt10 pt11]; % Transition Probability, tp01 = P(Y_t=1|Y_(t-1)=0)
    a0_1 = 3; b0_1 = 1; a1_1 =1; b1_1 = 3; abX1=[a0_1 b0_1;a1_1 b1_1];% Emission Probability for X(1) (Migration rate), a0_1 = alpha of Beta function for X_t(1) given Y_t=0
    a0_2 = 1; b0_2 = 3; a1_2 = 3; b1_2 = 1; abX2=[a0_2 b0_2;a1_2 b1_2]; % Emission Probability for X(2) (Happiness rate)
    numData=size(x,1); % adjustable
    %EMitr=12; % adjustable
    NMitr=EMitr; %NMitr=10; % adjustable
    Nm_threshold=0.001; % adjustable
    x=x+0.000000001;

    %% For recording all things
    tot_Fp0=zeros(EMitr,numData);
    tot_Fp1=zeros(EMitr,numData);
    tot_Bp0=zeros(EMitr,numData);
    tot_Bp1=zeros(EMitr,numData);
    tot_p0p1=zeros(EMitr,2);
    tot_Pt=zeros(EMitr,4);
    tot_abX1=zeros(EMitr,4);
    tot_abX2=zeros(EMitr,4);

    %% %%% EM algorithm iteration starts!!!
    for s=1:EMitr

       %% %% Forward-Backward Probability 
        Fp=zeros(numData,2);
        Bp_flip=zeros(numData-1,2);
        Bp=zeros(numData,2);

        Fp(1,1)=p0*betapdf(x(1,1),a0_1,b0_1)*betapdf(x(1,2),a0_2,b0_2)*10^100;
        Fp(1,2)=p1*betapdf(x(1,1),a1_1,b1_1)*betapdf(x(1,2),a1_2,b1_2)*10^100;
        Bp_flip(1,1)=betapdf(x(numData,1),a0_1,b0_1)*betapdf(x(numData,2),a0_2,b0_2)*10^100;
        Bp_flip(1,2)=betapdf(x(numData,1),a1_1,b1_1)*betapdf(x(numData,2),a1_2,b1_2)*10^100;
        for t=2:(numData-1)
            for i=1:2 % 'i=1' mean Y_t=0 i.e. Economic depression
                Fp(t,i)=betapdf(x(t,1),abX1(i,1),abX1(i,2))*betapdf(x(t,2),abX2(i,1),abX2(i,2))*( Pt(1,i)*Fp(t-1,1) + Pt(2,i)*Fp(t-1,2) );
                Bp_flip(t,i)= Pt(i,1)*Bp_flip(t-1,1)*betapdf(x(numData+1-t,1),abX1(1,1),abX1(1,2))*betapdf(x(numData+1-t,2),abX2(1,1),abX2(1,2)) + Pt(i,2)*Bp_flip(t-1,2)*betapdf(x(numData+1-t,1),abX1(2,1),abX1(2,2))*betapdf(x(numData+1-t,2),abX2(2,1),abX2(2,2));
            end
        end
        Fp(numData,1)=betapdf(x(numData,1),abX1(1,1),abX1(1,2))*betapdf(x(numData,2),abX2(1,1),abX2(1,2))*( Pt(1,1)*Fp(numData-1,1) + Pt(2,1)*Fp(numData-1,2) );
        Fp(numData,2)=betapdf(x(numData,1),abX1(2,1),abX1(2,2))*betapdf(x(numData,2),abX2(2,1),abX2(2,2))*( Pt(1,2)*Fp(numData-1,1) + Pt(2,2)*Fp(numData-1,2) );
        Bp=[flipud(Bp_flip);0,0];

        %% %% Maximization step
        %% % Updating 'Initial State Probability'
        p0= ( Fp(1,1)*Bp(1,1) ) / ( Fp(1,1)*Bp(1,1) + Fp(1,2)*Bp(1,2) );
        p1=1-p0;

        %% % Updating 'Transition Probability'
        pt00_Num=betapdf(x(numData,1),a0_1,b0_1)*betapdf(x(numData,2),a0_2,b0_2)*Fp(numData-1,1);
        pt00_Denom=Bp(numData-1,1)*Fp(numData-1,1);
        pt10_Num=betapdf(x(numData,1),a0_1,b0_1)*betapdf(x(numData,2),a0_2,b0_2)*Fp(numData-1,2);
        pt10_Denum=Bp(numData-1,2)*Fp(numData-1,2);
        for t=2:(numData-1)
            pt00_Num = pt00_Num + betapdf(x(t,1),a0_1,b0_1)*betapdf(x(t,2),a0_2,b0_2)*Bp(t,1)*Fp(t-1,1);
            pt00_Denom = pt00_Denom + Bp(t-1,1)*Fp(t-1,1);
            pt10_Num = pt10_Num + betapdf(x(t,1),a0_1,b0_1)*betapdf(x(t,2),a0_2,b0_2)*Bp(t,1)*Fp(t-1,2);
            pt10_Denum = pt10_Denum + Bp(t-1,2)*Fp(t-1,2);
        end
        pt00=( pt00*pt00_Num ) / pt00_Denom;
        pt01=1-pt00;
        pt10=( pt10*pt10_Num ) / pt10_Denum;
        pt11=1-pt10;

        %% % Updating 'Emission Probability for X(1) (Migration rate)' % Updating 'Emission Probability for X(2) (Happiness rate)'
        %% a0_1 = 2; b0_1 = 1;
    fgv0_1 = Fp(numData,1) / ( Fp(numData,1) + Fp(numData,2) );
    fc0_1 = Fp(numData,1)*log(x(numData,1)) / ( Fp(numData,1) + Fp(numData,2) );
    gc0_1 = Fp(numData,1)*log(1-x(numData,1)) / ( Fp(numData,1) + Fp(numData,2) );
    for t=1:(numData-1)
        fgv0_1 = fgv0_1 + ( Fp(t,1)*Bp(t,1) ) / ( Fp(t,1)*Bp(t,1) + Fp(t,2)*Bp(t,2) );
        fc0_1 = fc0_1 + ( Fp(t,1)*Bp(t,1)*log(x(t,1)) ) / ( Fp(t,1)*Bp(t,1) + Fp(t,2)*Bp(t,2) );
        gc0_1 = gc0_1 + ( Fp(t,1)*Bp(t,1)*log(1-x(t,1)) ) / ( Fp(t,1)*Bp(t,1) + Fp(t,2)*Bp(t,2) );
    end
    syms y z
    f0_1=fgv0_1*(psi(y+z)-psi(y))+fc0_1;
    g0_1=fgv0_1*(psi(y+z)-psi(z))+gc0_1;
    F0_1=(f0_1 + subs(f0_1,y,5-z))/fgv0_1;
    G0_1=(g0_1 + subs(g0_1,z,5-y))/fgv0_1;

    Nm0_1 = zeros(NMitr, 2);
    Nm0_1(1,:) = [a0_1, b0_1];
    for i=2:NMitr
        Nm0_1(i,:) = double( subs( [y, z] - [F0_1, G0_1]*inv(jacobian([F0_1, G0_1], [y, z])).', [y, z], Nm0_1(i-1,:)) );
        Jacob_var=double( subs( [F0_1, G0_1]*inv(jacobian([F0_1, G0_1], [y, z])).', [y, z], Nm0_1(i,:)) );
        if max(abs(Jacob_var(1)), abs(Jacob_var(2))) < Nm_threshold 
            a0_1=Nm0_1(i,1);
            b0_1=Nm0_1(i,2);
            break;
        end
    end
    if i==NMitr
        a0_1=Nm0_1(i,1);
        b0_1=Nm0_1(i,2);
    end
        
    %% a1_1 = 1; b1_1 = 2;
    fgv1_1 = Fp(numData,2) / ( Fp(numData,1) + Fp(numData,2) );
    fc1_1 = Fp(numData,2)*log(x(numData,1)) / ( Fp(numData,1) + Fp(numData,2) );
    gc1_1 = Fp(numData,2)*log(1-x(numData,1)) / ( Fp(numData,1) + Fp(numData,2) );
    for t=1:(numData-1)
        fgv1_1 = fgv1_1 + ( Fp(t,2)*Bp(t,2) ) / ( Fp(t,1)*Bp(t,1) + Fp(t,2)*Bp(t,2) );
        fc1_1 = fc1_1 + ( Fp(t,2)*Bp(t,2)*log(x(t,1)) ) / ( Fp(t,1)*Bp(t,1) + Fp(t,2)*Bp(t,2) );
        gc1_1 = gc1_1 + ( Fp(t,2)*Bp(t,2)*log(1-x(t,1)) ) / ( Fp(t,1)*Bp(t,1) + Fp(t,2)*Bp(t,2) );
    end
    syms y z
    f1_1=fgv1_1*(psi(y+z)-psi(y))+fc1_1;
    g1_1=fgv1_1*(psi(y+z)-psi(z))+gc1_1;
    F1_1=(f1_1 + subs(f1_1,y,5-z))/fgv1_1;
    G1_1=(g1_1 + subs(g1_1,z,5-y))/fgv1_1;

    Nm1_1 = zeros(NMitr, 2);
    Nm1_1(1,:) = [a1_1, b1_1];
    for i=2:NMitr
        Nm1_1(i,:) = double( subs( [y, z] - [F1_1, G1_1]*inv(jacobian([F1_1, G1_1], [y, z])).', [y, z], Nm1_1(i-1,:)) );
        Jacob_var=double( subs( [F1_1, G1_1]*inv(jacobian([F1_1, G1_1], [y, z])).', [y, z], Nm1_1(i,:)) );
        if max(abs(Jacob_var(1)), abs(Jacob_var(2))) < Nm_threshold 
            a1_1=Nm1_1(i,1);
            b1_1=Nm1_1(i,2);
            break;
        end
    end
    if i==NMitr
        a1_1=Nm1_1(i,1);
        b1_1=Nm1_1(i,2);
    end

    %% % Updating 'Emission Probability for X(2) (Happiness rate)'
    %% a0_2 = 1; b0_2 = 2;
    fgv0_2 = Fp(numData,1) / ( Fp(numData,1) + Fp(numData,2) );
    fc0_2 = Fp(numData,1)*log(x(numData,2)) / ( Fp(numData,1) + Fp(numData,2) );
    gc0_2 = Fp(numData,1)*log(1-x(numData,2)) / ( Fp(numData,1) + Fp(numData,2) );
    for t=1:(numData-1)
        fgv0_2 = fgv0_2 + ( Fp(t,1)*Bp(t,1) ) / ( Fp(t,1)*Bp(t,1) + Fp(t,2)*Bp(t,2) );
        fc0_2 = fc0_2 + ( Fp(t,1)*Bp(t,1)*log(x(t,2)) ) / ( Fp(t,1)*Bp(t,1) + Fp(t,2)*Bp(t,2) );
        gc0_2 = gc0_2 + ( Fp(t,1)*Bp(t,1)*log(1-x(t,2)) ) / ( Fp(t,1)*Bp(t,1) + Fp(t,2)*Bp(t,2) );
    end
    syms y z
    f0_2=fgv0_2*(psi(y+z)-psi(y))+fc0_2;
    g0_2=fgv0_2*(psi(y+z)-psi(z))+gc0_2;
    F0_2=(f0_2 + subs(f0_2,y,5-z))/fgv0_2;
    G0_2=(g0_2 + subs(g0_2,z,5-y))/fgv0_2;

    Nm0_2 = zeros(NMitr, 2);
    Nm0_2(1,:) = [a0_2, b0_2];
    for i=2:NMitr
        Nm0_2(i,:) = double( subs( [y, z] - [F0_2, G0_2]*inv(jacobian([F0_2, G0_2], [y, z])).', [y, z], Nm0_2(i-1,:)) );
        Jacob_var=double( subs( [F0_2, G0_2]*inv(jacobian([F0_2, G0_2], [y, z])).', [y, z], Nm0_2(i,:)) );
        if max(abs(Jacob_var(1)), abs(Jacob_var(2))) < Nm_threshold 
            a0_2=Nm0_2(i,1);
            b0_2=Nm0_2(i,2);
            break;
        end
    end
    if i==NMitr
        a0_2=Nm0_2(i,1);
        b0_2=Nm0_2(i,2);
    end
    
    %% a1_2 = 2; b1_2 = 1;
    fgv1_2 = Fp(numData,2) / ( Fp(numData,1) + Fp(numData,2) );
    fc1_2 = Fp(numData,2)*log(x(numData,2)) / ( Fp(numData,1) + Fp(numData,2) );
    gc1_2 = Fp(numData,2)*log(1-x(numData,2)) / ( Fp(numData,1) + Fp(numData,2) );
    for t=1:(numData-1)
        fgv1_2 = fgv1_2 + ( Fp(t,2)*Bp(t,2) ) / ( Fp(t,1)*Bp(t,1) + Fp(t,2)*Bp(t,2) );
        fc1_2 = fc1_2 + ( Fp(t,2)*Bp(t,2)*log(x(t,2)) ) / ( Fp(t,1)*Bp(t,1) + Fp(t,2)*Bp(t,2) );
        gc1_2 = gc1_2 + ( Fp(t,2)*Bp(t,2)*log(1-x(t,2)) ) / ( Fp(t,1)*Bp(t,1) + Fp(t,2)*Bp(t,2) );
    end
    syms y z
    f1_2=fgv1_2*(psi(y+z)-psi(y))+fc1_2;
    g1_2=fgv1_2*(psi(y+z)-psi(z))+gc1_2;
    F1_2=(f1_2 + subs(f1_2,y,5-z))/fgv1_2;
    G1_2=(g1_2 + subs(g1_2,z,5-y))/fgv1_2;

    Nm1_2 = zeros(NMitr, 2);
    Nm1_2(1,:) = [a1_2, b1_2];
    for i=2:NMitr
        Nm1_2(i,:) = double( subs( [y, z] - [F1_2, G1_2]*inv(jacobian([F1_2, G1_2], [y, z])).', [y, z], Nm1_2(i-1,:)) );
        Jacob_var=double( subs( [F1_2, G1_2]*inv(jacobian([F1_2, G1_2], [y, z])).', [y, z], Nm1_2(i,:)) );
        if max(abs(Jacob_var(1)), abs(Jacob_var(2))) < Nm_threshold 
            a1_2=Nm1_2(i,1);
            b1_2=Nm1_2(i,2);
            break;
        end
    end
    if i==NMitr
        a1_2=Nm1_2(i,1);
        b1_2=Nm1_2(i,2);
    end

        %% %% Let's record parameters!
        tot_Fp0(s,:)=Fp(:,1)';
        tot_Fp1(s,:)=Fp(:,2)';
        tot_Bp0(s,:)=Bp(:,1)';
        tot_Bp1(s,:)=Bp(:,2)';
        tot_p0p1(s,:)=[p0, p1];
        tot_Pt(s,:)=[pt00 pt01 pt10 pt11];
        tot_abX1(s,:)=[a0_1 b0_1 a1_1 b1_1];
        tot_abX2(s,:)=[a0_2 b0_2 a1_2 b1_2];
        Pt=[pt00 pt01;pt10 pt11];
        abX1=[a0_1 b0_1;a1_1 b1_1];
        abX2=[a0_2 b0_2;a1_2 b1_2];

    end
    %% %% EM algorithm iteration ends!!!

    %% %% Showing results
    Py0_x1x2 = zeros(1,numData); % P(Y_t=0 | X(1), X(2))
    for t=1:(numData-1)
        Py0_x1x2(t)= Fp(t,1)*Bp(t,1) / ( Fp(t,1)*Bp(t,1) + Fp(t,2)*Bp(t,2) );
    end
    Py0_x1x2(numData)= Fp(numData,1) / ( Fp(numData,1) + Fp(numData,2) );

save('Py0_x1x2','Py0_x1x2');

figure
[hAx,hLine1,hLine2] = plotyy(1:numData,log(Py0_x1x2),1:numData,params);
%title('Multiple Decay Rates')
hLine1.Marker = 'o';
hLine2.Marker = '*';
xlabel('Time t')
ylabel(hAx(1),'log P(Y_t=0 | X(1), X(2))') % left y-axis
ylabel(hAx(2),'Simulation Input') % right y-axis

figure
[hAx,hLine1,hLine2] = plotyy(1:numData,Py0_x1x2,1:numData,params);
%title('Multiple Decay Rates')
hLine1.Marker = 'o';
hLine2.Marker = '*';
xlabel('Time t')
ylabel(hAx(1),'P(Y_t=0 | X(1), X(2))') % left y-axis
ylabel(hAx(2),'Simulation Input') % right y-axis
end