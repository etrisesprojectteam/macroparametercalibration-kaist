function [revisedSRL,unhappyRatioAvg,numOfMovedPeople] = SegregationModel(simTime,worldSize,vacantRatio,populationRatio, simillarRatioList) 

%% ON/OFF Flags
    visOn = 1;
    
    jobSiteOn = 0;
    landPriceOn = 0;

%% Parameter Initialization
 
    
    neighborType = 1;
    unhappyRatioAvg = zeros(simTime,1);
    numOfMovedPeople = zeros(simTime,1);
    
    % LandPrice : Extended
    weightDiff = 0.02; % 현재 Agent가 위치하는 Cell을 기준으로 거리에 따른 가격 변동 weight차
    
%% Add Gaussian Noise    
    noiseDev = 0.1; % Variance of Normal Distribution. Mean = 0;
    rng('shuffle'); % Random Seed Generator
    noise = sqrt(noiseDev)*randn(1, simTime);
    
    revisedSRL = simillarRatioList + noise;
    
    for i = 1 : simTime
        if(revisedSRL(i) < 0)
            revisedSRL(i) = 0;
        elseif(revisedSRL(i) > 1)
            revisedSRL(i) = 1;
        else
            continue;
        end
    end

%% Run Segregation Simulation
	[world,price, vacantSet,numVacantCell] = getInitialRandomWorld(worldSize, vacantRatio, populationRatio);
    price = updatePrice(world, price, worldSize, weightDiff); % distributed price for initial world;
	moved = zeros(worldSize); % check moved agents in this iteration, renewed later.
    if(visOn)
        plotWorld(world); % Show Randomly initialized world of Schelling's Model
        colorbar;
        %figure;
        %fig2 = gcf;
        %image(price/max(max(price))*80, 'CDataMapping', 'scaled');    colorbar;
    end
            
    for t = 1:simTime % Running for designated simulation time (TODO, d(totalSatisfactory) < epsilon form)
        disp(t);
   
        simillarRatio = revisedSRL(t);

        disp(simillarRatio);
                
        % RandomItr Case
        idx1 = randperm(worldSize);
        idx2 = randperm(worldSize);
                
        for i = 1 : worldSize
            for j = 1 : worldSize
                x = idx1(mod(i+j,worldSize)+1); y = idx2(j);
                if(world(x,y) && ~moved(x,y)) %  어떤 agent가 존재하며, 해당 cell이 T=t_current에 변경된 적이 없다면
                    [happiness, unhappyRatio] = isHappy(x,y,world, world(x,y), simillarRatio, neighborType,worldSize);
                    happinessLand = isHappyLand(x,y,world,price,worldSize);
                    unhappyRatioAvg(t,1) = unhappyRatioAvg(t,1)+unhappyRatio; 
                    if(~happiness || ~happinessLand) % 해당 Agent가 현재 상황에 만족하지 않는다면 (주변 neighbor층이 맘에 들지 않거나, LandPrice가 자신의 기대보다 낮다면)
                        [candX,candY,vac, isCandExist] = getProferCell(x,y,world,price,vacantSet,numVacantCell,simillarRatio, neighborType, worldSize, happiness);
                        
                        if(isCandExist) % 이동할만한 후보지가 존재할 경우
                            moved(x,y) = 1;
                            moved(candX,candY) = 1;
                            temp = world(x,y);
                            world(x,y) = world(candX, candY);
                            world(candX, candY) = temp;

                            vacantSet(vac, 1) = x;
                            vacantSet(vac, 2) = y;
                            
                            price = updatePriceXY(x,y,world(x,y), price, worldSize, weightDiff);
                            price = updatePriceXY(candX,candY,world(candX,candY), price, worldSize, weightDiff);
                            numOfMovedPeople(t,1) = numOfMovedPeople(t,1)+1;
                        else
                            continue;
                        end
                    end
                end
            end
        end
        if(visOn)
            plotWorld(world);
            %figure
            %image(price/max(max(price))*80, 'CDataMapping', 'scaled');    colorbar;
        end
        moved = zeros(worldSize);
    end
    
%% Simulation Result Ploting
    figHandle = figure;
    subplot(1,2,1);
    plot(1:simTime,unhappyRatioAvg/(worldSize*worldSize*(1-vacantRatio)));
    title('Average of Personal Unhappiness');
    xlabel('Simulation Time');
    ylabel('Average of Personal Unhappiness');
    subplot(1,2,2);
    plot(1:simTime, numOfMovedPeople);
    title('Number of Moved Person');
    xlabel('Simulation Time');
    ylabel('Number of moved agents');

    figure
    image(price/max(max(price))*80, 'CDataMapping', 'scaled');    colorbar;
    
    unhappyRatioAvg= unhappyRatioAvg/(worldSize*worldSize*(1-vacantRatio));
    numOfMovedPeople= numOfMovedPeople/(worldSize*worldSize*(1-vacantRatio));
%% Export Experiment Data
    dlmwrite([strcat(pwd,'\ExperimentResult\') ,strcat('Experiment_',datestr(clock, 'MM_SS'),'.txt')], [simillarRatioList', unhappyRatioAvg, numOfMovedPeople], '\t');
end

