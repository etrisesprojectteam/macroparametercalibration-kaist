
%% Simulation Parameter Settings
close all;
clc;

simTime = 200;
worldSize = 10;
vacantRatio = 0.20;
populationRatio = 0.5;
numResultDim = 2;
replication = 30;

%% initial run to imitate the true value
% trueSimillarRatioList = [0.9*ones(1,simTime/5), 0.5*ones(1,simTime/5), 0.9*ones(1,simTime/5), 0.5*ones(1,simTime/5), 0.9*ones(1,simTime/5)];
% [params,trueUnhappy,trueMovement] = SegregationModel(simTime,worldSize,vacantRatio,populationRatio,trueSimillarRatioList);
% trueResult = [trueUnhappy trueMovement];


%% run multiple times to get the distribution
results = cell(replication,1);
for i = 1:replication
    estSimillarRatioList = [0.5*ones(1,simTime/5), 0.5*ones(1,simTime/5), 0.5*ones(1,simTime/5), 0.5*ones(1,simTime/5), 0.5*ones(1,simTime/5)];
    [params,estUnhappy,estMovement] = SegregationModel(simTime,worldSize,vacantRatio,populationRatio,estSimillarRatioList);
    results{i} = [estUnhappy estMovement];
end

%% statistical analyses on differences

avgResult = zeros(simTime,numResultDim);
stdResult = zeros(simTime,numResultDim);
for i = 1:simTime
    for j = 1:numResultDim
        temp = zeros(replication,1);
        for k = 1:replication
            result = results{k};
            temp(k) = result(i,j);
        end
        avgResult(i,j) = mean(temp);
        stdResult(i,j) = std(temp);
    end
end

figure;
for j = 1:numResultDim
    subplot(1,2,j);
    ciplot(avgResult(:,j)-2*stdResult(:,j),avgResult(:,j)+2*stdResult(:,j),1:simTime,[0.9 0.9 0.9]);
    hold on;
    plot(1:simTime,avgResult(:,j),'b-');
    hold on;
    plot(1:simTime,trueResult(:,j),'r-');
    if j == 1
        title('Average of Personal Unhappiness');
        xlabel('Simulation Time');
        ylabel('Average of Personal Unhappiness');
    else
        title('Number of Moved Person');
        xlabel('Simulation Time');
        ylabel('Number of moved agents');
    end
end

%% find the regime
dur_hypparams = {10,1};
obs_hypparams = {zeros(size(avgResult,2),1),eye(size(avgResult,2)),size(avgResult,2)+3,0.1};
estStateSeq = useHDPHSMM(size(avgResult,1), size(avgResult,2), avgResult',{0.5,0.5},obs_hypparams);
save('params','params')

idxState = [];
for i = 1:size(estStateSeq,2)
    estState = estStateSeq(i);
    flag = 0;
    for j = 1:length(idxState)
        if idxState(j) == estState
            flag = 1;
            break;
        end
    end
    if flag == 0
        idxState = [idxState estState];
    end
end

%% compare the parameters per regime

figure;
for j = 1:numResultDim
    subplot(numResultDim+2,1,j);
    plot(1:simTime,avgResult(:,j)-trueResult(:,j),'r-');
    hold on;
    if j == 1
        ylabel('Average of Personal Unhappiness');
    else
        ylabel('Number of moved agents');
    end
end
subplot(numResultDim+2,1,numResultDim+1);
image(estStateSeq);
subplot(numResultDim+2,1,numResultDim+2);
plot(1:simTime,estSimillarRatioList(:)-trueSimillarRatioList(:),'r-');
xlabel('Simulation Time');
ylabel('Parameter Difference');







% figure
% plot(1:25,params, 'g*-')
% xlabel('Time t');
% ylabel('Simulation Input');
% 
% 
% 
% figure
% [hAx,hLine1,hLine2] = plotyy(1:25,log(Py0_x1x2),1:25,params);
% %title('Multiple Decay Rates')
% hLine1.Marker = 'o';
% hLine2.Marker = '*';
% xlabel('Time t')
% ylabel(hAx(1),'log P(Y_t=0 | X(1), X(2))') % left y-axis
% ylabel(hAx(2),'Simulation Input') % right y-axis
% 
% 
% 
% figure
% [hAx,hLine1,hLine2] = plotyy(1:25,Py0_x1x2,1:25,params);
% %title('Multiple Decay Rates')
% hLine1.Marker = 'o';
% hLine2.Marker = '*';
% xlabel('Time t')
% ylabel(hAx(1),'P(Y_t=0 | X(1), X(2))') % left y-axis
% ylabel(hAx(2),'Simulation Input') % right y-axis