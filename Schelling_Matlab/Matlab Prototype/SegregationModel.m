function [revisedSRL,unhappyRatioAvg,numOfMovedPeople] = SegregationModel(simTime,worldSize,vacantRatio,populationRatio, simillarRatioList,vis) 

%% Parameter Initialization
 
    %simillarRatioList = [0.2,0.35,0.5,0.65,0.8];
    %simillarRatioList = [0.8,0.65,0.5,0.35,0.2];
    %simillarRatioList = [rand()*ones(1,25), rand()*ones(1,25), rand()*ones(1,25), rand()*ones(1,25), rand()*ones(1,25), rand()*ones(1,25), rand()*ones(1,25), rand()*ones(1,25),rand()*ones(1,25), rand()*ones(1,25), rand()*ones(1,25), rand()*ones(1,25),rand()*ones(1,25), rand()*ones(1,25), rand()*ones(1,25), rand()*ones(1,25),rand()*ones(1,25), rand()*ones(1,25), rand()*ones(1,25), rand()*ones(1,25),rand()*ones(1,25), rand()*ones(1,25), rand()*ones(1,25), rand()*ones(1,25),rand()*ones(1,25), rand()*ones(1,25), rand()*ones(1,25), rand()*ones(1,25),rand()*ones(1,25), rand()*ones(1,25), rand()*ones(1,25), rand()*ones(1,25),rand()*ones(1,25), rand()*ones(1,25), rand()*ones(1,25), rand()*ones(1,25),rand()*ones(1,25), rand()*ones(1,25), rand()*ones(1,25), rand()*ones(1,25)];
    neighborType = 1;
    unhappyRatioAvg = zeros(simTime,1);
    numOfMovedPeople = zeros(simTime,1);
    
%% Add Gaussian Noise    
    noiseDev = 0.1; % Variance of Normal Distribution. Mean = 0;
    %rng('default'); % Random Seed Generator
    noise = sqrt(noiseDev)*randn(1, simTime);
    
    revisedSRL = simillarRatioList + noise;
    
    for i = 1 : simTime
        if(revisedSRL(i) < 0)
            revisedSRL(i) = 0;
        elseif(revisedSRL(i) > 1)
            revisedSRL(i) = 1;
        else
            continue;
        end
    end

%% Run Segregation Simulation
	[world,vacantSet,numVacantCell] = getInitialRandomWorld(worldSize, vacantRatio, populationRatio);
	moved = zeros(worldSize);
    if vis == 1
        figure;
        plotWorld(world); % Show Randomly initialized world of Schelling's Model
    end
            
    for t = 1:simTime % Running for designated simulation time (TODO, d(totalSatisfactory) < epsilon form)
        disp(t);
   
        simillarRatio = revisedSRL(t);

        disp(simillarRatio);
                
        % RandomItr Case
        idx1 = randperm(worldSize);
        idx2 = randperm(worldSize);
                
        for i = 1 : worldSize
            for j = 1 : worldSize
                x = idx1(mod(i+j,worldSize)+1); y = idx2(j);
                if(~moved(x,y) && world(x,y)) % 해당 cell이 T=t_current에 변경된 적이 없고, 어떤 agent가 존재한다면
                    [happiness, unhappyRatio] = isHappy(x,y,world, world(x,y), simillarRatio, neighborType,worldSize);
                    unhappyRatioAvg(t,1) = unhappyRatioAvg(t,1)+unhappyRatio; 
                    if(~happiness) % 해당 Agent가 현재 상황에 만족하지 않는다면
                        numOfMovedPeople(t,1) = numOfMovedPeople(t,1)+1;
                        [candX,candY,vac] = getProferCell(x,y,world,vacantSet,numVacantCell,simillarRatio, neighborType, worldSize);
                                
                        moved(x,y) = 1;
                        moved(candX,candY) = 1;
                        temp = world(x,y);
                        world(x,y) = world(candX, candY);
                        world(candX, candY) = temp;
                                
                        vacantSet(vac, 1) = x;
                        vacantSet(vac, 2) = y;
                                
                        
                    end
                end
            end
        end
        if vis == 1
            clf;
            plotWorld(world);
            hold on;
        end
        moved = zeros(worldSize);
    end
    
%% Simulation Result Ploting
    if vis == 1
        figure;
        subplot(1,2,1);
        plot(1:simTime,unhappyRatioAvg/(worldSize*worldSize*(1-vacantRatio)));
        title('Average of Personal Unhappiness');
        xlabel('Simulation Time');
        ylabel('Average of Personal Unhappiness');
        subplot(1,2,2);
        plot(1:simTime, numOfMovedPeople);
        title('Number of Moved Person');
        xlabel('Simulation Time');
        ylabel('Number of moved agents');
    end
    
    unhappyRatioAvg= unhappyRatioAvg/(worldSize*worldSize*(1-vacantRatio));
    numOfMovedPeople= numOfMovedPeople/(worldSize*worldSize*(1-vacantRatio));
%% Export Experiment Data
    dlmwrite([strcat(pwd,'\ExperimentResult\') ,strcat('Experiment_',datestr(clock, 'MM_SS'),'.txt')], [simillarRatioList', unhappyRatioAvg, numOfMovedPeople], '\t');
end

