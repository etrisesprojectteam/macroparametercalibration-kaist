function [candX, candY, vac] = getProferCell( x,y, world, vacantSet,numVacantCell,simillarRatio,neighborType, worldSize) % (x,y) Agent가 이동할 수 있는 최적의 위치 확인
	len = zeros(numVacantCell);     len = len(1,:);
            
	for i = 1:length(vacantSet)
        if(isHappy(vacantSet(i,1),vacantSet(i,2),world,world(x,y),simillarRatio,neighborType,worldSize)) % 빈 칸 (world 전체 중 VacantSet)으로 이동했을 때, 행복한지 아닌지에 대한 boolean List
            len(i) = (abs(x-vacantSet(i,1)) + abs(y-vacantSet(i,2))); % 모든 빈 칸 Set에 대해 현재 이동하는 Agent와의 거리
        else
            len(i) = worldSize*2;
        end
	end % 결과로 나오는 len Vector는 이동했을때 행복한 vacant cell에 대해서만! 거리 표시하는 vector (나머지는 최대~값)
            
    candX = vacantSet(find(len==min(len), 1),1);
	candY = vacantSet(find(len==min(len), 1),2);
            
	vac = find(len==min(len), 1);
end