
%% Simulation Parameter Settings
close all;
clear;
clc;

addpath(genpath(fullfile(pwd,'../Schelling_Matlab')));
addpath(genpath(pwd));

simTime = 50;
worldSize = 10;
vacantRatio = 0.20;
populationRatio = 0.5;
numResultDim = 2;
replication = 10;
runTrueSimulationAgain = 1;
runEvolutionSimulationAgain = 1;
priorSimulationRunDataMatFile = 'SimulationResultStored.mat';
numExps = 30;
% rateExperimentExploration : Gaussian-->stdev multiplier, Beta-->alpha
% beta multiplier
rateExperimentExploration = 1;
% estimationDistribution : 1, Gaussian distribution; 2, Beta distribution
estimationDistribution = 2;
itrSelfEvolution = 20;

for j = 1:numExps
    estSimillarRatioLists{j} = [1/(numExps+1)*j*ones(1,simTime/5), 1/(numExps+1)*j*ones(1,simTime/5), 1/(numExps+1)*j*ones(1,simTime/5), 1/(numExps+1)*j*ones(1,simTime/5), 1/(numExps+1)*j*ones(1,simTime/5)];
end
trueSimillarRatioList = [0.9*ones(1,simTime/5), 0.1*ones(1,simTime/5), 0.9*ones(1,simTime/5), 0.1*ones(1,simTime/5), 0.9*ones(1,simTime/5)];

colors = {'r','b','g'};
for l = 1:numExps
    strLegend{l} = strcat('Case ',num2str(l));
end
showHDPHSMM = 0;
showSimResultDeviation = 0;
showSingleExpResult = 0;
showOverallExpResult = 1;
showEvolutionForIteration = 1;

tic

%% run true case simulation for a single run
if runTrueSimulationAgain == 0
    load(priorSimulationRunDataMatFile);
    runTrueSimulationAgain = 0;
else
    %% initial run to imitate the true value
    [params,trueUnhappy,trueMovement] = SegregationModel(simTime,worldSize,vacantRatio,populationRatio,trueSimillarRatioList,1);
    trueResult = [trueUnhappy trueMovement];
    runTrueSimulationAgain = 1;
end

storeParameterMean={};
storeResultDifference=zeros(itrSelfEvolution,numResultDim);
for itrloop = 1:itrSelfEvolution
    storeParameterMean{end+1} = zeros(1,simTime);
    for i = 1:simTime
        for j = 1:numExps
            storeParameterMean{end}(i) = storeParameterMean{end}(i) + estSimillarRatioLists{j}(i);
        end
        storeParameterMean{end}(i) = storeParameterMean{end}(i)/numExps;
    end
    %% run hypothesis experiments for replicated runs
    if runEvolutionSimulationAgain == 0
        load(priorSimulationRunDataMatFile);
        runEvolutionSimulationAgain = 0;
    else
        %% run multiple times to get the distribution
        expSets = cell(numExps,1);
        for j = 1:numExps
            results = cell(replication,1);
            for i = 1:replication
                [params,estUnhappy,estMovement] = SegregationModel(simTime,worldSize,vacantRatio,populationRatio,estSimillarRatioLists{j},0);
                results{i} = [estUnhappy estMovement];
            end
            expSets{j} = results;
        end
        save(priorSimulationRunDataMatFile);
        runEvolutionSimulationAgain = 1;
    end

    %% statistical analyses on differences
    avgResults = cell(numExps,1);
    stdResults = cell(numExps,1);
    for l = 1:numExps
        avgResult = zeros(simTime,numResultDim);
        stdResult = zeros(simTime,numResultDim);
        for i = 1:simTime
            for j = 1:numResultDim
                temp = zeros(replication,1);
                for k = 1:replication
                    result = expSets{l}{k};
                    temp(k) = result(i,2);
                end
                avgResult(i,j) = mean(temp);
                stdResult(i,j) = std(temp);
            end
        end
        avgResults{l} = avgResult;
        stdResults{l} = stdResult;
    end

    if showSimResultDeviation == 1
        for i = 1:numExps
            figure;
            for j = 1:numResultDim
                subplot(2,1,j);
                ciplot(avgResults{i}(:,j)-2*stdResults{i}(:,j),avgResults{i}(:,j)+2*stdResults{i}(:,j),1:simTime,[0.9 0.9 0.9]);
                hold on;
                plot(1:simTime,avgResults{i}(:,j),'b-');
                hold on;
                plot(1:simTime,trueResult(:,j),'r-');
                if j == 1
                    title(strcat('Output Result Distribution from Exp Case : ',num2str(i),' with ',num2str(replication),' reps.'));
                    %xlabel('Simulation Time');
                    ylabel('Average of Personal Unhappiness');
                else
                    %title('Number of Moved Person');
                    xlabel('Simulation Time');
                    ylabel('Number of moved agents');
                end
            end
        end
    end

    %% find the regime
    estStateSeqIntegs = zeros(numExps,simTime);
    for l = 1:numExps
        differences{l} = avgResults{l} - trueResult;
        dur_hypparams = {1,1};
        obs_hypparams = {zeros(size(differences{l},2),1),eye(size(differences{l},2)),size(differences{l},2)+3,0.1};
        estStateSeqs{l} = useHDPHSMM(size(differences{l},1), size(differences{l},2), differences{l}',dur_hypparams,obs_hypparams,showHDPHSMM);
        save('params','params')

        idxState = [];
        for i = 1:simTime
            estState = estStateSeqs{l}(i);
            estStateSeqIntegs(l,i) = estState;
            flag = 0;
            for j = 1:length(idxState)
                if idxState(j) == estState
                    flag = 1;
                    break;
                end
            end
            if flag == 0
                idxState = [idxState estState];
            end
        end
        idxStates{l} = idxState;
    end
    storeResultDifference(itrloop,:) = zeros(1,numResultDim);
    for l = 1:numExps
        storeResultDifference(itrloop,:) = storeResultDifference(itrloop,:) + sum(abs(differences{l}));
    end
    storeResultDifference(itrloop,:) = storeResultDifference(itrloop,:) / (numExps*sum(abs(trueResult)));
    
    
    %% compare the parameters per regime
    if showSingleExpResult == 1
        for l = 1:numExps
            figure;
            for j = 1:numResultDim
                subplot(numResultDim+2,1,j);
                plot(1:simTime,zeros(simTime,1),'k--');
                hold on;
                plot(1:simTime,avgResults{l}(:,j)-trueResult(:,j),'r-');
                hold on;
                if j == 1
                    title(strcat('Difference Regime Change from Exp Case : ',num2str(l),' with ',num2str(replication),' reps.'));
                    ylabel('Average of Personal Unhappiness');
                else
                    ylabel('Number of moved agents');
                end
            end
            subplot(numResultDim+2,1,numResultDim+1);
            image(estStateSeqs{l});
            subplot(numResultDim+2,1,numResultDim+2);
            plot(1:simTime,estSimillarRatioLists{l}(:)-trueSimillarRatioList(:),'r-');
            hold on;
            plot(1:simTime,zeros(simTime,1),'k--');
            xlabel('Simulation Time');
            ylabel('Parameter Difference');
        end
    end

    for l = 1:numExps
        likelihood{l} = ones(simTime,1);
        for i = 1:simTime
            for j = 1:numResultDim
                tempTrue = trueResult(i,j);
                tempMeanEst = avgResults{l}(i,j);
                tempStdEst = stdResults{l}(i,j);
                likelihood{l}(i) = likelihood{l}(i) * mvnpdf(tempTrue,tempMeanEst,tempStdEst+eps); 
            end
        end
    end
    for i = 1:simTime
        normalize = 0;
        for j = 1:numExps
            normalize = normalize + likelihood{j}(i);
        end
        for j = 1:numExps
            likelihood{j}(i) = likelihood{j}(i) / normalize;
        end
    end

    if showOverallExpResult == 1 
        figure;
        for j = 1:numResultDim
            subplot(numResultDim+3,1,j);
            for l = 1:numExps
                plot(1:simTime,avgResults{l}(:,j)-trueResult(:,j),colors{mod(l,3)+1});
                hold on;
            end
            plot(1:simTime,zeros(simTime,1),'k--');
            legend(strLegend);

            if j == 1
                title('Overall Difference Regime Change by Exp. Cases');
                ylabel({'Diff on Average of','Personal Unhappiness'});
            else
                ylabel({'Diff on Number of',' moved agents'});
            end
        end
        subplot(numResultDim+3,1,numResultDim+1);
        image(estStateSeqIntegs);
        subplot(numResultDim+3,1,numResultDim+2);
        for l = 1:numExps
            plot(1:simTime,estSimillarRatioLists{l}(:)-trueSimillarRatioList(:),colors{mod(l,3)+1});
            hold on;
        end
        plot(1:simTime,zeros(simTime,1),'k--');
        legend(strLegend);
        subplot(numResultDim+3,1,numResultDim+3);
        for l = 1:numExps
            plot(1:simTime,likelihood{l},colors{mod(l,3)+1});
            hold on;
        end
        legend(strLegend);
        xlabel('Simulation Time');
        ylabel({'Likelihood of','Observed Result'});
    end

    %% find the calibration candidate points
    uniqueStateSignatures = {};
    uniqueStateTimeIndex = {};
    for i = 1:simTime
        flag = 0;
        for j = 1:length(uniqueStateSignatures)
            if isequal(uniqueStateSignatures{j},estStateSeqIntegs(:,i)) == 1
                uniqueStateTimeIndex{j} = [uniqueStateTimeIndex{j} i];
                flag = 1;
                break;
            end
        end
        if flag == 0
            uniqueStateSignatures{end+1} = estStateSeqIntegs(:,i);
            uniqueStateTimeIndex{end+1} = [i];
        end
    end
    
    if estimationDistribution == 1
        estNextSimulationParams = zeros(1,simTime);
        estNextSimulationParamsStd = zeros(1,simTime);
        for i = 1:length(uniqueStateTimeIndex)
            timeindexes = uniqueStateTimeIndex{i};
            avgWeightedParamPerRegime = 0;
            avgWeightPerRegime = 0;
            stdWeightPerRegime = 0;
            for j = 1:length(timeindexes)
                for k = 1:numExps
                    avgWeightedParamPerRegime = ...
                        avgWeightedParamPerRegime + likelihood{k}(timeindexes(j)) * estSimillarRatioLists{k}(timeindexes(j));
                    avgWeightPerRegime = avgWeightPerRegime + likelihood{k}(timeindexes(j));
                    stdWeightPerRegime = stdWeightPerRegime + likelihood{k}(timeindexes(j))*likelihood{k}(timeindexes(j));
                end
            end
            avgWeightPerRegime = avgWeightPerRegime / ( numExps * length(timeindexes));
            stdWeightPerRegime = stdWeightPerRegime / ( numExps * length(timeindexes));
            stdWeightPerRegime = stdWeightPerRegime - avgWeightPerRegime*avgWeightPerRegime;
            stdWeightPerRegime = sqrt(stdWeightPerRegime);
            avgWeightedParamPerRegime = avgWeightedParamPerRegime / length(timeindexes);
            for j = 1:length(timeindexes)
                estNextSimulationParams(timeindexes(j)) = avgWeightedParamPerRegime;
                estNextSimulationParamsStd(timeindexes(j)) = stdWeightPerRegime;
            end
        end

        estSimillarRatioLists = {};
        for j = 1:numExps
            explorationPointDesign = j-(numExps+1)/2;
            for i = 1:simTime
                estSimillarRatioLists{j}(i) = estNextSimulationParams(i) + explorationPointDesign*rateExperimentExploration/estNextSimulationParamsStd(i);
            end
        end
    elseif estimationDistribution == 2
        estNextSimulationParamsAlphaBeta = zeros(2,simTime);
        for i = 1:length(uniqueStateTimeIndex)
            timeindexes = uniqueStateTimeIndex{i};
            provedPoints = zeros(length(timeindexes)*numExps,1);
            provedLikelihoods = zeros(length(timeindexes)*numExps,1);
            for j = 1:length(timeindexes)
                for k = 1:numExps
                    provedPoints( (j-1)*numExps + k ) = estSimillarRatioLists{k}(timeindexes(j));
                    provedLikelihoods( (j-1)*numExps + k ) = likelihood{k}(timeindexes(j));
                end
            end
            [alpha,beta] = beta_para_est(provedPoints',provedLikelihoods');
            for j = 1:length(timeindexes)
                estNextSimulationParamsAlphaBeta(1,timeindexes(j)) = alpha;
                estNextSimulationParamsAlphaBeta(2,timeindexes(j)) = beta;
                
                estNextSimulationParams(timeindexes(j)) = alpha / (alpha+beta);
                estNextSimulationParamsStd(timeindexes(j)) = sqrt( (alpha*beta)/((alpha+beta)*(alpha+beta)*(alpha+beta+1)) );
            end
        end

        estSimillarRatioLists = {};
        for j = 1:numExps
            for i = 1:length(uniqueStateTimeIndex)
                timeindexes = uniqueStateTimeIndex{i};
                provedPointNext = 0;
                while provedPointNext < 0.001 || provedPointNext > 0.999
                    provedPointNext = betarnd(rateExperimentExploration*estNextSimulationParamsAlphaBeta(1,timeindexes(1)),...
                        rateExperimentExploration*estNextSimulationParamsAlphaBeta(2,timeindexes(1)));
                end
                for k = 1:length(timeindexes)
                    estSimillarRatioLists{j}(timeindexes(k)) = provedPointNext;
                end
            end
        end
    end
    
    if showEvolutionForIteration == 1
        figure;
        subplot(4,1,1);
        for l = 1:numExps
            plot(1:simTime,likelihood{l},colors{mod(l,3)+1});
            hold on;
        end
        title(strcat({'Self Evolution Parameters : Iteration '},{num2str(itrloop)}));
        legend(strLegend);
        ylabel({'Likelihood of','Observed Result'});
        subplot(4,1,2);
        plot(1:simTime,estNextSimulationParams,'b');
        hold on;
        plot(1:simTime,trueSimillarRatioList,'r--');
        ylabel({'Simulation Parameter'});
        legend('Next Iteration Parameter','True Parameter');
        subplot(4,1,3);
        for j = 1:numExps
            plot(1:simTime,estSimillarRatioLists{j},colors{mod(j,3)+1});
            hold on;
        end
        ylabel({'Simulation Parameter'});
        legend(strLegend);
        
        subplot(4,1,4);
        plot(1:simTime,estNextSimulationParamsStd);
        ylabel({'Standard Deviation of','Likelihood'});
        xlabel('Simulation Time');
    end
    
    parameterDifference(itrloop) = sum(abs(estNextSimulationParams-trueSimillarRatioList))/sum(trueSimillarRatioList);
    drawnow;
end

figure;
plot(1:itrSelfEvolution,parameterDifference);
xlabel('Self Evolution Iteration');
ylabel('Parameter Deviation');

figure;
for i = 1:numResultDim
    subplot(numResultDim,1,i);
    plot(1:itrSelfEvolution,storeResultDifference(:,1));
    xlabel('Self Evolution Iteration');
    ylabel('Result Difference');
end

figure;
plot(1:simTime,trueSimillarRatioList,'r');
hold on;
xlabel('Simulation Time');
ylabel('Parameter');
ylim([-0.3 1.3]);
h1 = plot(1:simTime,storeParameterMean{1},'b');
title(strcat({'Self Evolution Parameters : Iteration '},{num2str(1)}));
hold on;
h2 = -1;
for i = 2:length(storeParameterMean)
    pause(2);
    if h2 ~= -1
        delete(h2);
    end
    delete(h1);
    h1 = plot(1:simTime,storeParameterMean{i},'b');
    hold on;
    h2 = plot(1:simTime,storeParameterMean{i-1},'color',[0.5 0.5 0.5]);
    hold on;
    title(strcat({'Self Evolution Parameters : Iteration '},{num2str(i)}));
end
toc














