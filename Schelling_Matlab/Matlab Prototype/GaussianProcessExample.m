snr = 0.2;
numMaxPoints = 400;

X = 0:0.1:2*3.14;
numTruePoints = length(X);
trueY = sin(X);

theta = [1 1 1 1];
beta = 100;

figure;
for itr2 = 1:numMaxPoints
    sampleX(itr2) = 2*3.14*rand(1,1);
    sampleY(itr2) = sin(sampleX(itr2)) + snr * randn(1,1);
    numPoints = length(sampleX);
    
    inputs = 0:0.3:2*3.14;
    for itr1 = 1:length(inputs)
        inputElement = inputs(itr1);

        %%% Place a call for function on learning the hyperparameter beta
        %%% and theta

        C = zeros(numPoints,numPoints);
        for i = 1:numPoints
            for j = 1:numPoints
                C(i,j) = KernelFunction(theta, sampleX(i) ,sampleX(j));
                if i == j 
                    C(i,j) = C(i,j) + 1/beta;
                end
            end
        end

        k = zeros(numPoints,1);
        for i = 1:numPoints
            k(i) = KernelFunction(theta, sampleX(i), inputElement);
        end
        c = KernelFunction(theta, inputElement, inputElement) + 1/beta;

        C_inv = inv(C);
        mu_next(itr1) = k'*C_inv*sampleY';
        sigma2_next(itr1) = c - k'*C_inv*k;
    end
    
    clf;
    ciplot(mu_next-2*sqrt(sigma2_next),mu_next+2*sqrt(sigma2_next),inputs,[0.9 0.9 0.9]);
    hold on;
    plot(sampleX,sampleY,'r*');
    hold on;
    plot(X,trueY,'g-');
    hold on;
    plot(inputs,mu_next,'bo-');
    prompt = 'Hit Enter for Next Sample';
    temp = input(prompt);
end



