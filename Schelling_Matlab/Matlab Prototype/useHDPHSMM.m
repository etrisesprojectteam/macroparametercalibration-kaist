function [ estState ] = useHDPHSMM( length,numDimensions,observationData,dur_hypparams,obs_hypparams,showHDPHSMM )
    % data should be row = dimnesion, col = length
    %% parameter setting
    T = length;
    obs_dim = numDimensions;

    % inference
    Nmax = 10; % weak limit approximation parameter
    Niter = 100;
    plot_results = 1;
    plot_results_every = 10;

    %% create posterior model
    for state=1:Nmax
        obs_distns{state} = observations.gaussian(obs_hypparams{:});
        dur_distns{state} = durations.poisson(dur_hypparams{:});
    end

    posteriormodel = hsmm(T,obs_distns,dur_distns);

    %% do posterior inference
    if showHDPHSMM == 1
        fig = figure();
    end

    for iter=1:Niter
        posteriormodel.resample(observationData);
        util.print_dot(iter,Niter);
        if showHDPHSMM == 1
            if plot_results && (mod(iter,plot_results_every) == 0)
                posteriormodel.plot(observationData,fig);
                title(sprintf('SAMPLED at iteration %d',iter));
                drawnow
            end
        end
    end

    estState = posteriormodel.states.stateseq;

end

