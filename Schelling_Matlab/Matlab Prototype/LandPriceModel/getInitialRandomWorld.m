function [initworld, initPrice, vacantSet, numVacantCell] = getInitialRandomWorld(worldSize, vacantRatio, populationRatio)
	numVacantCell = 0;
    numPop1 = 0;
            
    size = worldSize;
	initworld = zeros(size);
    initPrice = 100*ones(size);
            
	idx1 = randperm(size);
	idx2 = randperm(size);
            
	for i = 1:size
        for j = 1:size
            if(rand(1) > vacantRatio)
                if(rand(1) > populationRatio)
                    initworld(idx1(i),idx2(j)) = 1;
                    numPop1 = numPop1 + 1;
                else
                    initworld(idx1(i),idx2(j)) = 2;
                end
            else
                numVacantCell = numVacantCell +1;
                vacantSet(numVacantCell,:) = [idx1(i),idx2(j)];
            end
        end
    end
	realVacantRatio = numVacantCell / (size*size)
	realPopulationRatio = numPop1 / (size*size)
end % Initial Random World Loader