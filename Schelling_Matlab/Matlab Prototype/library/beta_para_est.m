function [alpha,beta] = beta_para_est(X,weight)
    % normalize the weight
    weight = weight / sum(weight);
    m = sum(weight);

    new_parameter = [0.1;0.1]; %[alpha,beta]
    num_itr = 20;
    G = zeros(2,2);
    g = zeros(2,1);
    
    for itr = 1:num_itr        
        alpha = new_parameter(1);
        beta = new_parameter(2);
        % Newton raphson method
        g(1) = psi(0 , alpha) - psi(0,alpha+beta) - sum(weight .* log(X))/m;
        g(2) = psi(0 , beta) - psi(0,alpha+beta) - sum(weight .* log(1-X))/m;
        G(1,1) = psi(1,alpha)-psi(1,alpha+beta);
        G(1,2) = -psi(1,alpha+beta);
        G(2,1) = G(1,2);
        G(2,2) = psi(1,beta)-psi(1,alpha+beta); 
        new_parameter = new_parameter - G\g;
    end
    alpha = new_parameter(1);
    beta = new_parameter(2);
end

%% Run script
%{
clear
clc
close all

randn('seed',100)
rand('seed',100)
true_alpha = 2; %0.5;
true_beta = 5; %0.5;
n = 9990 %10000; 9990
data = betarnd(true_alpha,true_beta,n,1);
weight = ones(n,1);
idx_weight = 0.4 < data & data < 0.5;
weight(idx_weight) = 100;
[alpha,beta] = beta_para_est(data,weight);
%}
