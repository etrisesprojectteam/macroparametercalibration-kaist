classdef Agent < handle
    % 160310 Schelling's Segregation Model
    % Parent Agent Class, dhkim
    properties
        cellPosition % Environment 상에서 Agent의 위치
        character % Agent의 특성
        happiness % 현재 위치 상의 neighbor에 대한 행복 여부
    end
    
    methods
        function this = Agent(pos, char, happy)
            if nargin == 0
                this.cellPosition = [0,0];
                this.character = 0;
                this.happiness = 0;
            end
            
            this.cellPosition = pos;
            this.character = char;
            this.happiness = happy;
        end
        
        function setPosition(this, pos)
            this.cellPosition = pos;
        end
        
        function setCharacter(this, char)
            this.character = char;
        end
        
        function setHappiness(this, happy)
            this.happiness = happy;
        end
        
        function pos = getPosition(this)
            pos = this.cellPosition;
        end
        
        function char = getCharacter(this)
            char = this.character;
        end
        
        function happy = getHappiness(this)
            happy = this.happiness;
        end
        
        function happy = calculatedHappiness(this, env)
        end
    end
end
