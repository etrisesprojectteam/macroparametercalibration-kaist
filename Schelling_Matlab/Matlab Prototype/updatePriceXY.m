function price = updatePriceXY(x,y,agentType, inPrice, size, priceWeight)
    price = inPrice;
    currentPos = (y-1)*size+x;
    distOneNeigh = [currentPos-1, currentPos+1, currentPos-size, currentPos+size];
    distTwoNeigh = [currentPos-size-1, currentPos-size+1, currentPos+size-1, currentPos+size+1];
    
    if(x == 1)
        distOneNeigh(1) = -size-1; distTwoNeigh([1,3]) = -size-1;
    end
    
    if(y == 1)
        distOneNeigh(3) = -size-1; distTwoNeigh([1,2]) = -size-1;
    end
    
    if(x == size)
        distOneNeigh(2) = -size-1; distTwoNeigh([2,4]) = -size-1;
    end
    
    if(y == size)
        distOneNeigh(4) = -size-1; distTwoNeigh([3,4]) = -size-1;
    end
    
    distOneNeigh(find(distOneNeigh == -size-1)) = [];
    distTwoNeigh(find(distTwoNeigh == -size-1)) = [];
    
    if(agentType == 1)
        price(currentPos) = (1 + 3*priceWeight) * inPrice(currentPos);
        price(distOneNeigh) = (1 + 2*priceWeight) * inPrice(distOneNeigh);
        price(distTwoNeigh) = (1 + priceWeight) * inPrice(distTwoNeigh);
    elseif(agentType == 2)
        price(currentPos) = (1 - 3*priceWeight) * inPrice(currentPos);
        price(distOneNeigh) = (1-2*priceWeight) * inPrice(distOneNeigh);
        price(distTwoNeigh) = (1-priceWeight) * inPrice(distTwoNeigh);
    elseif(agentType == 0)
        price(currentPos) = inPrice(currentPos);
        price(distOneNeigh) = inPrice(distOneNeigh);
        price(distTwoNeigh) = inPrice(distTwoNeigh);
    end
end
